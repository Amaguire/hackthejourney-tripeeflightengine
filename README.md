

# ExpressTripee

Tripee Application runs on a node server 

npm install 
npm start

## Usage
Inspiration Flight Search Engine from multiple origins. 
User inputs multiple origins and the engine will find destinations in common. 


##Application
The application requires an Amadeus client and secret password to be added in app.js file

Due to the json responses from API calls do not always contain city names, calls are made to mongodb database, 
where a schema of all airport names and Iata codes. 

In order for the search engine to display, the schema in required to be uploaded to mongodb. 

The search engine uses Amadeus inspiration search for the first call, not all locations are currently supported so some destinations may return an error

At the moment the amount of destinations are limited in the search.controller file. This can be modified as required. The reason it is currently limited is due to the time it would take to filter through all the destinations and then find prices for each, bearing in mind the timeout consideration for the Api calls. 
An improvement in the pipeline is to reduce the time required to perform the search and increase performance. 

## Developing

**server side**

bin/www- port configurations 

package.json - module dependencies and  proxy configuration

app.js - file is main express code which connects and routes 

api/controller/search.controllers.js - makes calls to API and writes results to file 

api/controller/destination.controllers.js - generates the JSON objects from file 

routes/index.js - Routes to corresponding controller 

**client side**

client/src/index.js - reads the HTML file and routes components

client/src/views/containers- contains all components such as Home, search, shopping cart



### Tools

Created with [Nodeclipse](https://github.com/Nodeclipse/nodeclipse-1)
 ([Eclipse Marketplace](http://marketplace.eclipse.org/content/nodeclipse), [site](http://www.nodeclipse.org))   

Nodeclipse is free open-source project that grows with your contributions.
