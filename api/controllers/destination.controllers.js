/**
 * This file generates the new JSON objects and returns restructured data
 */

var fs = require('fs');
var DestinationData = require('../data/destinations3.json');
var DestinationData2 = require('../data/destinations4.json');
var DestinationList = require('../data/destinationList');
var express = require('express');
var bodyParser = require('body-parser');
var parseJson = require('parse-json');
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
var destList = require('dest-list-tripee');
var origin = require('tripee-origins');
var db;

/**
 * This is the connection to mongoDB database
 * @type {*|Mongoose}
 */
var mongoose = require('mongoose');
mongoose.promise= global.Promise;
mongoose.connect('mongodb://localhost/Tripee',  { useNewUrlParser: true },  function (err, database) {
    if (err) throw err;
    else {
        db= database;
        console.log('Successfully connected to mongodb');
    }
});

/**
 * This function takes the data from JSON file and restructures the data and returns it to router
 * @param req
 * @param res
 */

module.exports.destination1GetPrice = function(req, res) {
    console.log('GET prices');

    var FinalArrival;
    var FirstDeparture;
    var Price;
    var routeId;
    var FirstDateTimeArrival;
    var FirstDateTimeDepart;
    var  SecondDateTimeArrival;
    var  SecondDateTimeDepart;
    var durationDirectOrLegOne;
    var durationLeg2;
    var firstLegavailabilty;
    var secondLegavailabilty;
    var firstArrival;
    var FirstDepartCityName;
    var secondDeparture;
    var FinalArrivalCityName;
    var  PriceArrayFinal = [];
   var cityNameMapArray=[];
   var cityOriginNameMapArray =[];
   var CityNameOrigin=[];
    var DestListArray=[];
    var OriginArray=[];
    var FlightDataStringify;
    var myMap;
    var CityName;
   var Longitude;
   var Latitude;
    var countryCode;
    var dateTimeParse;
    var dateTimeParseDepart;
    var Durationparse1;
    var Durationparse2;
    var destdata = (DestinationData);
    var ParsedDate;
    var destdata2 = (DestinationData2);
    var listofDest = {DestinationList};
    var priceArray = [];
    var priceArray2 = [];
    var parseArrTimeND;
    var parseDepartTimeND;
    var finalArrival2;
    var FirstDeparture2;


    for (let i = 0, len = destdata.length; i < len; i++) {
        var priceElement = (destdata);
        priceArray.push(priceElement[i]);

    }

    for (let i = 0, len = destdata2.length; i < len; i++) {
        var priceElement2 = (destdata2);
        priceArray2.push(priceElement2[i]);

    }


    /**
     * This for loop iterates through to find the list of destinations.
     * Checks if the route is direct or non direct by the depth of flight segment array
     */
    for (let i = 0, len = destdata.length; i < len; i++) {

        var DestElement = destdata;

        var destObj = Object.keys(DestElement[i].offerItems[0].services[0].segments);
        var DestListArrayList={};

        if (destObj.hasOwnProperty('1')) {
            finalArrival2 = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;
            DestListArrayList = (finalArrival2);
        }else{
            finalArrival2 = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
            DestListArrayList = (finalArrival2);
        }

        DestListArray.push(DestListArrayList);

    }

    /**
     * This for loop iterates through to find the list of origins.
     */
    for (let i = 0, len = destdata.length; i < len; i++) {

        var DestElement2 = destdata;

        var destObj2 = Object.keys(DestElement2[i].offerItems[0].services[0].segments);
        var OriginArrayList={};

            FirstDeparture2 = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
            OriginArrayList = (FirstDeparture2 );


        OriginArray.push(OriginArrayList);

    }


    /**
     * origin and destination lists are added to sets to remove duplicates
     */

    var s1= new Set(DestListArray);
    var s2= new Set(OriginArray);
    myMap = new Map();

    /**
     * This function wrapped in promise so this code will excute before the following
     * the Destination set is iterated through
     * The mongoDB query inputs the IATA code from the set iteration
     * Returns the city name and adds to array
     * @type {Promise<any>}
     */
    var CityPromise3 = new Promise(function (resolve, reject) {

        for (let value of s1) {
            db.collection("AirportData").find({iata_code: value}).toArray(function (err, result) {
                if (err) throw err;

                //console.log(result[0].municipality);
                CityName = result[0].municipality;

                    console.log(CityName);
                cityNameMapArray.push(CityName);
                resolve(cityNameMapArray);

            });

        }

    });


CityPromise3.then(function(){
    console.log(cityNameMapArray);
});

    /**
     * This function wrapped in promise so this code will excute before the following
     * the origin set is iterated through
     * The mongoDB query inputs the IATA code from the set iteration
     * Returns the city name and adds to array
     * @type {Promise<any>}
     */
    var CityOriginPromise = new Promise(function (resolve, reject) {

        for (let value of s2) {
            db.collection("AirportData").find({iata_code: value}).toArray(function (err, result) {
                if (err) throw err;

                //console.log(result[0].municipality);
                CityNameOrigin = result[0].municipality;

                cityOriginNameMapArray.push(CityNameOrigin);
                resolve(cityOriginNameMapArray, CityNameOrigin);

            });

        }

    });

    /**
     * Function converts ISO time format
     * source = https://stackoverflow.com/questions/20518516/how-can-i-get-time-from-iso-8601-time-format-in-javascript/20518554#20518554
     * @param n
     * @returns {string}
     * @constructor
     */
    function ConvertNumberToTwoDigitString(n) {
        return n > 9 ? "" + n : "0" + n;
    }

    CityOriginPromise.then(function () {
        console.log('CityNme' + cityOriginNameMapArray);
    });

    /**
     * This is the main loop which iterates through the JSON file, extracts the data and restructures it ,
     * adding information queried from the data base
     *
     */
    for (let i = 0, len = destdata.length; i < len; i++) {

            var DestElement = destdata;
            var destObj = Object.keys(destdata[i].offerItems[0].services[0].segments);
            var FlightData;

        // this checks the array depth to decipher if direct or non direct route
        if (destObj.hasOwnProperty('1')) {
            var CityPromise = new Promise(function (resolve, reject) {
                FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;
                firstArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;


                //mongoDB query with first departure IATA code to city Name an
                db.collection("AirportData").find({iata_code: FirstDeparture}).toArray(function (err, result) {
                    if (err) throw err;

                    FirstDepartCityName = result[0].municipality;
                    console.log(" IN DATABASE CALLS"+FirstDepartCityName);


                });
                //mongoDB query with Final Arrival IATA code to get Final Arrival city name and longitudde and latitude codes
                db.collection("AirportData").find({iata_code: FinalArrival}).toArray(function (err, res2) {
                    if (err) throw err;
                    FinalArrivalCityName  = res2[0].municipality;
                    Longitude= res2[0].longitude_deg;
                    Latitude= res2[0].latitude_deg;
                    resolve();


                });

            });


            /**
             * Once the data is retrieved from Data base, the JSON objects are constructed
             */
            CityPromise.then(function () {
                    FlightData = {};

                    routeId = DestElement[i].id;
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;


                    //Date time for depart and arrival on first and second Leg
                     dateTimeParse = new Date(DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at);
                    ParsedDate = (dateTimeParse.getDay()) + " - " + (dateTimeParse.getMonth()) + " - " + (dateTimeParse.getFullYear());


                    FirstDateTimeArrival = ConvertNumberToTwoDigitString(dateTimeParse.getUTCHours()) +
                        ":" + ConvertNumberToTwoDigitString(dateTimeParse.getUTCMinutes());

                    dateTimeParseDepart = new Date(DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at);

                    FirstDateTimeDepart = ConvertNumberToTwoDigitString(dateTimeParseDepart.getUTCHours()) +
                        ":" + ConvertNumberToTwoDigitString(dateTimeParseDepart.getUTCMinutes());

                    //LEG ONE DURATION
                    Durationparse1 = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                    Durationparse1.split("T");
                    var split = Durationparse1.split("T");
                    var split2 = split[1].split("H");
                    var split3 = split2[1].split("M");
                    durationDirectOrLegOne = split2[0] + " Hrs " + split3[0] + " mins";

                    //LEG TWO DURATION
                    Durationparse2 =  DestElement[i].offerItems[0].services[0].segments[1].flightSegment.duration;
                    Durationparse2.split("T");
                    var split2ndLeg = Durationparse2.split("T");
                    var split22ndLeg = split2ndLeg[1].split("H");
                    var split32ndLeg = split22ndLeg[1].split("M");
                    durationLeg2 = split22ndLeg[0] + " Hrs " + split32ndLeg[0] + " mins";

                    //PARSED DEPARTURE AND ARRIVAL
                    parseArrTimeND= new Date(DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.at);
                    parseDepartTimeND= new Date(DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.at);
                    SecondDateTimeArrival =ConvertNumberToTwoDigitString(parseArrTimeND.getUTCHours()) +
                        ":" + ConvertNumberToTwoDigitString(parseArrTimeND.getUTCMinutes());
                    SecondDateTimeDepart = ConvertNumberToTwoDigitString(parseDepartTimeND.getUTCHours()) +
                        ":" + ConvertNumberToTwoDigitString(parseDepartTimeND.getUTCMinutes());


                    secondDeparture = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.iataCode;
                    firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;
                    secondLegavailabilty = DestElement[i].offerItems[0].services[0].segments[1].pricingDetailPerAdult.availability;
                    Price = DestElement[i].offerItems[0].price.total;
                    // console.log('true');


                    //JSON OBJECT CONSTRUCTION
                    FlightData["FlightID"] = (routeId);
                    FlightData["Departure"] = (FirstDeparture);
                    FlightData["FirstDepartureTime"] = (FirstDateTimeDepart);
                    FlightData["FirstArrival"] = (firstArrival);
                    FlightData["FirstArrivalTime"] = (FirstDateTimeArrival);
                    FlightData["firstLegDuration"] = (durationDirectOrLegOne);
                    FlightData["routeAvailabilityFirst"] = (firstLegavailabilty);

                    FlightData["SecondLegDeparture"] = (secondDeparture);
                    FlightData["SecondDepartureTime"] = (SecondDateTimeDepart);
                    FlightData["FinalArrival"] = (FinalArrival);
                    FlightData["FinalArrivalTime"] = (SecondDateTimeArrival);
                    FlightData["SecondLegDuration"] = (durationLeg2);
                    FlightData["routeAvailability"] = (secondLegavailabilty);
                    FlightData["Price"] = (Price);
                    FlightData["FirstDepartCityName"] = (FirstDepartCityName);
                    FlightData["FinalArrivalCity"] = (FinalArrivalCityName);
                    FlightData["LongitudeCode"] = (Longitude);
                    FlightData["LatitudeCode"] = (Latitude);


                    //JSON OBJECTS PUSHED TO ARRAY
                    PriceArrayFinal.push(FlightData);

                }

                )} else {

            /**
             * This is for Direct routes and construction of JSON - if array length is 0
             * @type {Promise<any>}
             */
                var CityPromise2 = new Promise(function (resolve, reject) {

                FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;


                //mongoDB query with first departure IATA code to city Name an
                var originCityOne = db.collection("AirportData").find({iata_code: FirstDeparture}).toArray(function (err, result) {
                    if (err) throw err;
                    FirstDepartCityName = result[0].municipality;
                    countryCode = result[0].iso_country;

                });

                //mongoDB query with Final Arrival IATA code to get Final Arrival city name and longitudde and latitude codes
                db.collection("AirportData").find({iata_code: FinalArrival}).toArray(function (err, result) {
                    if (err) throw err;
                    FinalArrivalCityName = result[0].municipality;
                    Longitude= result[0].longitude_deg;
                    Latitude= result[0].latitude_deg;
                    resolve();

                });

            });



            CityPromise2.then(function () {

                FlightData = {};

                routeId = DestElement[i].id;
                FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
                FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                Price = DestElement[i].offerItems[0].price.total;

               dateTimeParse = new Date(DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at);
               ParsedDate= (dateTimeParse.getDay())+" - "  + (dateTimeParse.getMonth()) + " - " + (dateTimeParse.getFullYear());

                //PARSED DATES
                FirstDateTimeArrival = ConvertNumberToTwoDigitString(dateTimeParse.getUTCHours()) +
                    ":" + ConvertNumberToTwoDigitString(dateTimeParse.getUTCMinutes());
                dateTimeParseDepart= new Date(DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at );
                FirstDateTimeDepart = ConvertNumberToTwoDigitString(dateTimeParseDepart.getUTCHours()) +
                    ":" + ConvertNumberToTwoDigitString(dateTimeParseDepart.getUTCMinutes());

                Durationparse1= DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                Durationparse1.split("T");
                var split= Durationparse1.split("T");
               var split2 = split[1].split("H");
               var split3= split2[1].split("M");
                durationDirectOrLegOne = split2[0]+" Hrs "+ split3[0] +" mins";
                firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;

                //CONSTRUCT THE JSON OBJECTS FOR DIRECT ROUTE
                FlightData["FlightID"] = (routeId);
                FlightData["Departure"] = (FirstDeparture);
                FlightData["FirstDepartureTime"] = (FirstDateTimeDepart);
                FlightData["Date"]=(ParsedDate);
                FlightData["FinalArrival"] = (FinalArrival);
                FlightData["FinalArrivalTime"] = (FirstDateTimeArrival);
                FlightData["Price"] = (Price);
                FlightData["firstLegDuration"] = (durationDirectOrLegOne);
                FlightData["routeAvailability"] = (firstLegavailabilty);
                FlightData["FirstDepartCityName"] = (FirstDepartCityName);
                FlightData["FinalArrivalCity"] = (FinalArrivalCityName);
                FlightData["LongitudeCode"]=(Longitude);
                FlightData["LatitudeCode"]=(Latitude);


                //JSON OBJECT PUSHED TO ARRAY
                PriceArrayFinal.push(FlightData);

                FlightDataStringify = JSON.stringify(PriceArrayFinal);
                console.log(FlightData);


            });


           }


    };


    /**
     * GROUP BY METHOD- adpated method seen on stack overflow https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-a-array-of-objects
     * @param list
     * @param Location
     * @returns {Map<any, any>}
     */
    function groupBy(list, Location) {
        const map = new Map();
        list.forEach((item) => {
            const key = Location(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
        return map;
    }




    var FA= [];
    var FA2=[];
    var Total=[];
    var T=[];
    
    /**
     * s1 is the set of destinations
     * Firstly the reconstructed data is filtered by destination
     * And then filtered by origins
     * If either of the origin has empty array for a destination then the destination is removed 
     */
    setTimeout(function () {
        var FilterPromise = new Promise(function (resolve, reject) {
            var s3 = new Set(cityOriginNameMapArray);
            console.log("filter Promise");
            for (let value of s1) {
                console.log('value' + value);
                const routeGrouped4 = groupBy(PriceArrayFinal, route => route.FinalArrival);
                console.log(value + ': ' + routeGrouped4.get(value));


                for (let origin of s3) {
                    const originGrouped2 = groupBy(routeGrouped4.get(value), route => route.FirstDepartCityName);

                    if (originGrouped2.get(origin) === undefined) {
                        s1.delete(value);
                        console.log("deleting error");
                    }
                    console.log("Found routes");
                }
            }

            /**
             * s1 is the set of destinations
             * Firstly the reconstructed data is filtered by destination
             * and then filtered by origin
             * Constructed into the correct Array structure and then sent to the client
             */

            for (let value of s1) {
                console.log('value' + value);
                const routeGrouped = groupBy(PriceArrayFinal, route => route.FinalArrival);
                console.log(value + ': ' + routeGrouped.get(value));

                var originArray = [];

                for (let origin of s3) {
                    console.log("new s1" + s1.toString());
                    console.log('origin' + origin);
                    console.log(FirstDepartCityName);
                    console.log("depart" + FirstDeparture);


                    //add in if origin exisits then group by - if not continue/delete

                    const originGrouped = groupBy(routeGrouped.get(value), route => route.FirstDepartCityName);
                    console.log('group by :' + value + 'route by ' + FirstDepartCityName);
                    console.log(origin + ': ' + originGrouped.get(origin));


                    var originT = originGrouped.get(origin);

                    originArray.push(originT);

                }

                FA2.push(originArray);
                console.log(originArray);


            }
            FA.push(FA2);

            Total.push(FA);
            console.log(Total);
        });
        
    FilterPromise.then(
            res.send(Total));

    }, 12000);


};



