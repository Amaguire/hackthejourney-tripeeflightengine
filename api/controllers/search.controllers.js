/**
 * This file is the Search controller which carries out API calls to fetch destinations for the origins selected by user

 * @type {createError}
 * @type {createApplication}
 *  @type {module:path}
 *  @type {cookieParser}
 *  @type {Parsers|*}
 *   @type {module:fs}
 */
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var destinationList = require('dest-list-tripee');
var origin = require('tripee-origins');
var flightRoutes = require('routes-tripee');
var fs = require('fs');


/**
 * production credentials for Amadeus
 */
var Amadeus = require('amadeus');
var amadeus = new Amadeus({
    hostname: 'production',
    clientId:    'WMA79ejcMbxGuWjhMgAVR6HqCG8oe0km',
    clientSecret: 'WLpXRkHZt9hzOksI'
});

/**
 * @type {app}
 */
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // to support
// URL-encoded bodies


/**
 * This function receives the users origins and makes API calls to find destinations in common
 *
 */
module.exports.SearchRoutes = function(req, res, next) {

    var priceSearch1Array = [];
    var flatData;
    var FlightData = new Promise(function (resolve, reject) {

        var OrigOne;
        var OrigTwo;

        var destOne;
        var destTwo;

        var DesListOne;
        var DesListTwo;

        var iataCode1;
        var iataCode2;

        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        var promise;
        var promise2;


        console.log(req.body);
        //request.body is the data obtained from react
        //originOne and originTwo are the user inputs
        var parsed1 = req.body.originOne;
        var parsed2 = req.body.originTwo;
        var date = req.body.date;
        var route = req.body.direct;


        // This parses the date and turns it into ISO format of which the API requires
        var dateParsed = new Date(date);
        var dateISO = dateParsed.getFullYear() + '-' + ('0' + (dateParsed.getMonth() + 1)).slice(-2) + '-' + ('0' + dateParsed.getDate()).slice(-2);



        //parse the input eg. Madrid, Spain => Madrid
        var split1 = parsed1.split(',');
        var split2 = parsed2.split(',');
        console.log(split1[0]);
        console.log(split2[0]);

        OrigOne = split1[0];
        OrigTwo = split2[0];

        console.log('This is setting the values : ' + OrigOne + ' And ' + OrigTwo);

        //following method with the parased origin values passed through as parameters in the method
        IataCodeSearchOrg1(OrigOne, OrigTwo);


        /**
         * This function makes an API call with the city name to retreive the IATA code
         * The origin module sets the result
         * Error handled and returned to express
         * @param OrigOne
         * @param OrigTwo
         * @constructor
         */
        function IataCodeSearchOrg1(OrigOne, OrigTwo) {

            amadeus.referenceData.locations.get({
                keyword: OrigOne,
                subType: Amadeus.location.any
            }).then(function (response) {

                console.log(response.data[0].iataCode);
                iataCode1 = response.data[0].iataCode;
                origin.setFirstOrigin(iataCode1);

                //next method called- api call for the second origin Iata code
                IataCodeSearchOrg2(OrigTwo, iataCode1);

            }).catch(function (error) {
                console.log(error.response); // => The response object with
                console.log("this is an City name to IATA code error");
                return next(error); // Handle this error

            });

        }
        /**
         * This function makes an API call with the city name to retreive the IATA code
         * The origin module sets the result
         * Error handled and returned to express
         * @param OrigOne
         * @param OrigTwo
         * @constructor
         */
        function IataCodeSearchOrg2(OrigTwo, iataCode1) {

            amadeus.referenceData.locations.get({
                keyword: OrigTwo,
                subType: Amadeus.location.any
            }).then(function (response) {

                iataCode2 = response.data[0].iataCode;
                origin.setSecondOrigin(iataCode2);

                //function called
                firstOrigin(iataCode1, iataCode2);


            }).catch(function (error) {
                console.log(error.response);
                return next(error);
            })

        }

        /**
         * This function makes a call to API using IATA code to find all destinations that it flies to
         * If error returned from API, error is returned to app.js where it is handled
         * @param iataCode1
         * @param iataCode2
         */
        function firstOrigin(iataCode1, iataCode2) {

            amadeus.shopping.flightDestinations.get({
                origin: iataCode1

            }).then(function (response) {

                destOne = response.result.dictionaries.locations;
                flightRoutes.setFirstDest(destOne);
                var routeResult = flightRoutes.getRouteInfo();

                secondOrigin(iataCode1, iataCode2);

            }).catch(function (error) {
                return next(error);

            })
        }

        /**
         * This function makes a call to API using IATA code to find all destinations that it flies to
         * If error returned from API, error is returned to app.js where it is handled
         * @param iataCode1
         * @param iataCode2
         */
        function secondOrigin(iataCode1, iataCode2) {
            //delay(1000);
            amadeus.shopping.flightDestinations.get({

                origin: iataCode2

            }).then(function (response) {

                destTwo = response.result.dictionaries.locations;

                flightRoutes.setSecondDest(destTwo);

                var SecRouteResult = flightRoutes.getSecRouteInfo();
                compareDestinations(iataCode1, iataCode2);

            }).catch(function (error) {
                console.log(error.response); // => The response object with//
                console.log(error.code); // => A unique error code to identify//
                error.shouldRedirect = true;
                // the// type of error
                return next(error);

            });


        }

        /**
         * This function compares both lists of destinations and returns a compatible list
         * @param iataCode1
         * @param iataCode2
         */
        function compareDestinations(iataCode1, iataCode2) {

            //gets each list of destinations
            var destList = flightRoutes.getRouteInfo();
            var SecDestList = flightRoutes.getSecRouteInfo();

            //from JSON to string
            var JstringOne = JSON.stringify(destList);
            var JstringTwo = JSON.stringify(SecDestList);
            //getting values of object
            var List = destList.routeDestOne;
            var List2 = SecDestList.routeDestTwo;

            //gets only the keys of both list - eg : MAD, AMS, LON
            var keySetOne = Object.keys(List);
            var keySetTwo = Object.keys(List2);

            //both lists of keys are put in sets
            var s1 = new Set(keySetOne);
            var s2 = new Set(keySetTwo);

            //if either origin is present in the list- delete
            s1.delete(iataCode1);
            s2.delete(iataCode2);

            /**
             * This  compareLists function is adapted from a method found on http://xahlee.info/js/js_set_union.html
             * @param sets
             * @returns {*|Set<any>}
             */
            //method with compare both sets and adds any destinations which are in both to a new set
            const compareLists = (...sets) => sets.reduce(
                (Org1, Org2) => {
                    var X = new Set();
                    Org2.forEach((v => {
                        if (Org1.has(v)) X.add(v)
                    }));
                    return X;
                });


            var listCompared = compareLists(s1, s2);
            destinationList.setDestList(listCompared);


            finalDest(iataCode1, iataCode2);

        }

        var code1;
        var code1parsed;
        var code2parsed;
        var code2;
        var priceSearch2Array = [];

        /**
         * This function gets the destination list generated and adds it to an array
         * The array is iterated through with the findPrices() function called within the loop
         * The timeout promises are due to API constraints
         * The array can be slice and limited to avoid making too many calls if destination list was over 100/ taking too much time
         * @param iataCode1
         * @param iataCode2
         */

        function finalDest(iataCode1, iataCode2) {

             var finalResult = destinationList.getDestList();

            code1 = origin.getOriginInfo();
            code1parsed = code1.routeOriginOne;

            var DestinationL;
            var destLoop = finalResult.destinationList;
            var i, destArray = [];
            destArray = Array.from(destLoop);
            var limitDestArray = destArray.slice(0, 6);



            var result;
            for (let i = 0, len = limitDestArray.length; i < len; i++) {
                setTimeout(() => {

                    DestinationL = destArray[i];
                    result += findPrices(code1parsed, DestinationL);


                    if (i === len) {
                        setTimeout(() => {

                        }, 500);
                    }
                }, i * 500);

            }

            setTimeout(function () {
                finalDest2(iataCode1, iataCode2)
            }, 7000);


        }

        /**
         * This function gets the destination list generated and adds it to an array
         * The array is iterated through with the findPrices() function called within the loop
         * The timeout promises are due to API constraints
         * The array can be slice and limited to avoid making too many calls if destination list was over 100/ taking too much time
         *
         * Once all the API responses are added to array, the array is flattened and written to JSON file
         * The set timeout around writing to file is to ensure the array is not sent to file before the api requests are complete
         * @param iataCode1
         * @param iataCode2
         */
        function finalDest2(iataCode1, iataCode2) {

            console.log('second origin search');
            var finalResult = destinationList.getDestList();


            console.log('logging final result' + finalResult);


            code2 = origin.getSecOriginInfo();
            code2parsed = code2.routeOriginTwo;
            console.log('code 2 parsed value' + code2parsed);

            var DestinationL;
            var destLoop = finalResult.destinationList;


            var i, destArray = [];
            destArray = Array.from(destLoop);
            var limitDestArray = destArray.slice(0, 6);

            // console.log("Dest Array size: " + destArray.length);
            console.log(JSON.stringify(limitDestArray));

            for (let i = 0, len = limitDestArray.length; i < len; i++) {
                setTimeout(() => {
                    console.log('in 2nd loop');
                    DestinationL = destArray[i];

                    findPrices2(code2parsed, DestinationL);


                    if (i === len) {
                        setTimeout(() => {

                        }, 500);
                    }
                }, i * 500);
            }

            setTimeout(function () {


                console.log(priceSearch1Array);
                var f = JSON.stringify(...priceSearch1Array);


                const flatten = dataIn => dataIn.reduce((prev, curr) => [
                    ...prev,
                    ...curr
                ]);

                    console.log('writingto file');
                    flatData = flatten(priceSearch1Array);
                    res.send();
                    fs.writeFileSync('api/data/destinations3.json', JSON.stringify(flatData));
                    resolve();

            }, 7000);
        }


        var priceSearchVar;
        var priceSearchVar2;
        var storePriceVar;


        /**
         * This function finds the prices against origin One
         * The parsed information departure data and if the route is direct or non direct
         * Origin A refers to the originOne the user selected
         * Destination B is variable of the destination list iteration
         * @param A
         * @param B
         * @returns {Promise<any>}
         */
        function findPrices(A, B) {

            promise = new Promise(function (resolve) {

                (amadeus.shopping.flightOffers.get({
                    origin: A,
                    destination: B,
                    departureDate: dateISO,
                    nonStop: route

                }).then(function (response) {

                    priceSearchVar = response.result.data;
                    priceSearch1Array.push(priceSearchVar);
                    console.log(priceSearchVar);
                    destinationList.setPriceSearch1(priceSearchVar);
                    storePriceVar = destinationList.getPriceSearch1();

                    resolve('success');

                }).catch(function (error) {
                    console.log('error and continue' + error);

                    resolve('resolving');
                    console.log('resolving');

                }))
            }, 300);
            return promise;
        }

        /**
         * This function finds the prices against origin Two
         * The parsed information departure data and if the route is direct or non direct
         * Origin A refers to the origin Two the user selected
         * Destination B is variable of the destination list iteration
         * @param A
         * @param B
         * @returns {Promise<any>}
         */
        function findPrices2(A, B) {

            promise2 = new Promise(function (resolve) {
                (amadeus.shopping.flightOffers.get({
                    origin: A,
                    destination: B,
                    departureDate: dateISO,
                    nonStop: route

                }).then(function (response) {
                    priceSearchVar2 = response.result.data;

                    priceSearch1Array.push(priceSearchVar2);
                    console.log(priceSearchVar);

                    resolve('success');

                }).catch(function (error) {
                    resolve('resolving second search error');
                    console.log(error.code);
                }))
            }, 300);

            return promise2;

        }
    });

    };
