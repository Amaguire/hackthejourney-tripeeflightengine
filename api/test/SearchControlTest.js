var mocha= require('mocha');

var assert = require('assert');
var SearchController =  require('../controllers/search.controllers.js');
var http_mocks= require('node-mocks-http');

var expect = require('chai').expect;
var should = require('chai').should();

var chai = require('chai');



function ResponseBuild(){
    return http_mocks.createResponse({eventEmitter: require('events').EventEmitter});
}

describe('get flight Data', function(){

    it('should return flight data', function(done){
        this.timeout(20000);
       var res = ResponseBuild();
       var req = http_mocks.createRequest({
           method:'GET',
           url:'/search'

       });

    res.on('end', function(){

        res._getData().should.an(object);
        done();
    })

    })
});