
let chrome = require('selenium-webdriver/chrome');
let chromedriver = require('chromedriver');

var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());

var driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

driver.get('http://localhost:3000/');

//driver.findElement(By.name('FlightForm')).sendKeys('webdriver');


//driver.findElement(By.name('search')).click();
//driver.findElement(By.name('search')).click();

driver.sleep(2000).then(function() {
    driver.getTitle().then(function(title) {
        if(title === 'Tripee') {
            console.log('Test passed');
        } else {
            console.log('Test failed');
        }
        driver.quit();
    });
});