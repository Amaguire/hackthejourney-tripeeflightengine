var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var routes = require('./api/routes');

var bodyParser = require('body-parser');
var parseJson = require('parse-json');
var objectToJson = require('object-to-json');
var nodefilter = require('node-json-filter');
var destinationList = require('dest-list-tripee');
var delay = require('delay');
var await = require('await');
var origin = require('tripee-origins');
var flightRoutes = require('routes-tripee');
var fs = require('fs');


if (typeof localStorage === "undefined" || localStorage === null) {
	  var LocalStorage = require('node-localstorage').LocalStorage;
	  localStorage = new LocalStorage('./scratch');
	}

var db;
 var mongoose = require('mongoose');
 mongoose.promise= global.Promise; 
 mongoose.connect('mongodb://localhost/Tripee',  { useNewUrlParser: true },  function (err, database) {
	    if (err) throw err;
	    else { 
		   db= database;
	   console.log('Successfully connected to mongodb');
	   }
	});


var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // to support
														// URL-encoded bodies
/*/*
//test amadeus
var Amadeus = require('amadeus');
 var amadeus = new Amadeus({
	// hostname: 'production',
    clientId:    'S197I2kaGs8XDgyvx7wPv9kwpShtvGi2',
     clientSecret: 'ipCGQBatkmgARpHp'
 });

*/

//production
var Amadeus = require('amadeus');
var amadeus = new Amadeus({
	hostname: 'production',
    clientId:    '',
     clientSecret: '',
 });


//app.use(middleware);
app.use('/', routes);
app.use('/search', routes);

//error handler
 app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
 
// view engine setup
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-jquery')('/jquery.js'));


const middleware = [

	express.static(path.join(__dirname, 'public')),
];

app.use(function(err, req, res, next ) {
	// render the error page
	console.log("ERROR REDIRECT");

	res.status(500).send({"Error" : err.stack});


});




module.exports = app;
 
