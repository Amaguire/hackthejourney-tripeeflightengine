import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import { Provider } from 'react-redux';
import {DestinationPrices} from './views/containers/DestinationPrices';
import {originCards} from "./views/containers";
import {NavCart} from './views/containers/NavCart';
import {NavBar} from './views/containers/NavBar';
// Store
import store from './store/index';


// display views 
import {App} from './views/components/index';
import {Home} from './views/containers/Home';


// routes the components to page
import Routes from './services/routes';

ReactDOM.render(<Routes />, document.getElementById('root'));

