import React from 'react';
import { BrowserRouter as Router, Route, Link  } from 'react-router-dom';
import {DestinationPrices} from '../views/containers/DestinationPrices';
import {App} from "../views/components";
import {Home} from '../views/containers/Home';
import {booking} from '../views/containers/booking';
import {loader} from '../views/containers/loader';
import {error} from '../views/containers/error';
import {form} from '../views/containers/form';

const Routes = () => (
    <Router>
        <App>
        <Route exact path ='/' component ={Home}/>
        <Route exact path ='/search' component ={DestinationPrices}/>
        <Route exact path = '/booking' component ={booking}/>
                <Route exact path = '/searching' component ={loader}/>
                <Route exact path = '/error' component ={error}/>
                <Route exact path = '/form' component ={form}/>
        </App>
    </Router>
);

export default Routes;

