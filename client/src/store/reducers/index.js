import { SET_LOCATION_TEXT_INPUT_ORIGIN_ONE,SET_LOCATION_TEXT_INPUT_ORIGIN_TWO, SET_DEFAULT_INPUT,
  LOADING_FLIGHT_DATA, LOADED_FLIGHT_DATA, ERROR_FLIGHT_DATA } from '../actions/index';

const defaultState = {
  locationData: '',
  locationData2: '',
  locationType: 'IATA',
  loading: true,
  loaded: false,
  error: false,
};

const rootReducer = function(state = defaultState, action) {
  const { payload } = action;

  switch (action.type) {
    case SET_LOCATION_TEXT_INPUT_ORIGIN_ONE: {
      return { ...state, locationData: payload.locationData }; 
    }
    case SET_LOCATION_TEXT_INPUT_ORIGIN_TWO: {
      return { ...state, locationData2: payload.locationData2 };
    }
    case SET_DEFAULT_INPUT: {
      return { ...state };
    }
    case LOADING_FLIGHT_DATA: {
      return { ...state, loading: true, loaded: false, error: false };
    }
    case LOADED_FLIGHT_DATA: {
      const { main, price, name, time } = payload;

      return { ...state, loading: false, loaded: true, main: main, price:price, name: name, time: time };
    }
    case ERROR_FLIGHT_DATA: {
      return { ...state, loading: false, error: true };
    }
    default: {
      return { ...state };
    }
  }
};

export default rootReducer;
