import { default as App } from './App';

import { default as Loader } from './Loader';

export { App, Loader };
