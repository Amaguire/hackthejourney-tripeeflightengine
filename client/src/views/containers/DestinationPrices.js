/**
 * This file renders the search page results
 */

import React from 'react';
import { CardImg, CardText, CardBody,
    CardTitle, CardSubtitle } from 'reactstrap';
import Card from '@material-ui/core/Card';
import { Container, Row, Col } from 'reactstrap';
import ClipLoader from 'react-spinners/ClipLoader';
import MapContainer from './map';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FaGlobeAmericas} from 'react-icons/fa';
import {FaPlane} from 'react-icons/fa';
import {FaClock} from 'react-icons/fa';
import {FaCloud} from 'react-icons/fa';
import {NavCart} from './NavCart';
import Checkbox from "@material-ui/core/Checkbox";
import {css} from "@emotion/core";

const override = css`
    display: block;
    margin: 0 auto;
    border-color: white;
`;


const AnyReactComponent = ({ text }) => <div>{text}</div>;




export class DestinationPrices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flightData: [],
            isChecked: props.isChecked || false,
            data:'',
            cart:[],
            google:'',
            loading:true

        };

        this.handleCheckboxChange= this.handleCheckboxChange.bind(this);
        this.loading=this.loading.bind(this);
    }


    /**
     * This function fetches the data to display
     * While waiting for data the spinner is defaulted to true, once the data is fetched the spinner is set to false
     */
    componentDidMount() {
                  fetch('/search')
                .then(res => {
                    return res.json()

                })
                .then(res => {

                    console.log(res.toString());
                    this.setState({flightData: res});
                    this.setState({loading:false})
                })
                .catch(error => console.log(error));



    }

    /**
     * This function updates the basket and removes item if already present in basket
     * @param flight
     */
    removeItemFromBasket(flight) {
        const items = this.state.cart.filter(x=> x.FlightID !== flight.FlightID);

        this.setState({ flight })
    }

    /**
     * This method updates the state of the basket
     * I asked a question on stack overflow regarding this issue
     * https://stackoverflow.com/questions/55648358/how-to-remove-unchecked-items-from-cart-state-in-react
     * @param item
     */
    handleCheckboxChange = item => {
        this.setState(prevState => {
            const isItemInCart = prevState.cart.some(el => el.FlightID === item.FlightID);
            const cart = isItemInCart
                ? prevState.cart.filter(el => el.FlightID !== item.FlightID)
                : [...prevState.cart, item];

            return { cart };
        });
    };


    originTitleTwo() {
        var data = this.state.flightData;

        return data.map(FD => (
            <Card >
                <Card.body>
                    <Card.Title>
                        {FD[0].FirstDepartCityName}
                    </Card.Title>
                </Card.body>
            </Card>



            )
        )
    }


    loading() {
          return (
                <div className='sweet-loading'>
                    <ClipLoader
                       css={override}
                        sizeUnit={"px"}
                        size={300}
                        color={'#28bcb5'}
                        loading={this.state.loading}
                    />
                </div>
            )
        }

    /**
     * This function adds the parameters to the map from the data fetched
     * @param FD
     * @returns {*}
     */
    destMap(FD) {

        console.log(FD);
        var data3= FD[0];

        console.log("latitude code"+data3.LatitudeCode);
        console.log("longitude code"+data3.LongitudeCode);
        return (

                <Card >
                    <MapContainer

                        id={data3.FlightID}
                        options={{
                            center: { lat: data3.LatitudeCode, lng: data3.LongitudeCode },
                            zoom: 6
                        }}
                        onMapLoad={map => {
                            var marker = new window.google.maps.Marker({
                                position: { lat: data3.LatitudeCode, lng: data3.LongitudeCode },
                                map: map,
                                title: 'Hello Istanbul!'
                            });
                        }}
                    />

                </Card>




        )
    }

    /**
     * Nav cart displayed
     * @returns {*}
     * @constructor
     */
    NavCart() {

        return (

            <div  className="text-center"  style={{float: "right" }}>
                <Card body className="text-center" style={{maxWidth: 375, minWidth:375 , paddingTop: 10}}>

                    <CardTitle ><h3><b>Flights Selected : </b></h3></CardTitle>
                    <CardText ><span className="fa fa-cart-plus fa-2x"> {this.props.cart.length}</span></CardText>


                </Card>

            </div>

            )

    }

    /**
     * selecting the but on the route card
     * @param FD
     * @returns {*}
     * @constructor
     */
   Select(FD) {

       return (
           <div>
            <label>

                <Checkbox id={FD.FlightID}
                          name={FD.FlightID}
                          //onClick={(()=>this.handleClick(FD))}
                          //checked={this.setState.route}
                          value={this.state.isChecked}
                          onChange={(()=> this.handleCheckboxChange(FD))}


                />
                <span>Select</span>
            </label>
           </div>


        )
    }

    createCards() {
        var data = this.state.flightData;



        return data.map(FD => (

                <div style={{paddingTop:5}} >
                    {this.createPanels(FD)}
                </div>

            )
        )
    }

    /**
     * Destination panels
     * @param FD
     * @returns {*}
     */
    createPanels(FD) {


        var data= FD[0];

        console.log(" CODES LAT AND LONG"+FD[0][0][0].LongitudeCode, FD[0][0][0].LatitudeCode);
        return data.map(FS => (


                // let finalDest = "";

                    <ExpansionPanel className="text-center" >
                        <ExpansionPanelSummary className="text-center" style={{paddingTop:5}} >

                            <Typography style={{paddingLeft:525}} className="text-center" ><h2><FaGlobeAmericas/><b>{FS[0][0].FinalArrivalCity}</b></h2></Typography>

                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>


                            <div className="w3-third-center">
                            <Col style={{float:'left', paddingLeft:20, paddingRight:15 , minWidth: 400}} >

                                {this.createCardsOriginOne(FS)}
                            </Col>
                        </div>
                            <div className="w3-third-center"  style={{ paddingTop :10, minWidth: 400}}>
                                <Col  >
                                    <Card style={{minWidth:350}}>
                                        {this.mapCont(FS[0][0])}

                                    </Card>
                                </Col>
                            </div>

                            <div className="w3-third-center" style={{float:'right' ,paddingLeft:20, paddingRight:5, minWidth: 400}}>
                                <Col style={{float:'right'}}>

                                    {this.createCardsOriginTwo(FS)}
                                </Col>
                            </div>

                        </ExpansionPanelDetails>
                    </ExpansionPanel>

            )
        )
    }

    /**
     * Map
     * @param FS
     * @returns {*}
     */
    mapCont(FS){

        var MapCoords= FS;

        console.log("map identifier"+MapCoords.FinalArrival);
        return(
            <MapContainer

                id={MapCoords.FinalArrival}
                options={{
                    center: { lat:  parseFloat(MapCoords.LatitudeCode), lng:parseFloat(MapCoords.LongitudeCode)},
                    zoom: 6
                }}
                onMapLoad={map => {
                    var marker = new window.google.maps.Marker({
                        position: { lat:  parseFloat(MapCoords.LatitudeCode), lng:parseFloat(MapCoords.LongitudeCode)},
                        map: map,

                    });
                }}
            />

        )
    }

    /**
     * Destination routes  for Origin One
     * @param FS
     * @returns {*}
     */
    createCardsOriginOne(FS){

        console.log(FS);
        var data3= FS[0];
        console.log(data3);
        if (data3.FirstArrival!== undefined){
            console.log('Yes');

        }else{
            console.log('NO!');
        }

        return data3.map( FD1 => (

                <Row style={{paddingTop: 10, paddingLeft:10, paddingRight:10}}> <Card style={{backgroundColor:'#4dd0e1'}} className="card" key={FD1.FlightID}>
                    <div className="text-center" style={{minWidth:350}}>
                        <h5 ><b> {FD1.FirstDepartCityName}</b></h5>
                        <div className="panel panel-default datasource_panel">
                            <div className="panel-heading">

                                <Card body className="text-center">

                                    <CardTitle data-es-label="location2" style={{paddingTop:5}}>
                                        <b> {FD1.Departure} </b>.....  <FaPlane/><b> {FD1.FirstArrival} </b><FaPlane/>..... <b>{FD1.FinalArrival} </b>   </CardTitle>


                                    <CardText data-es-label="Price">{FD1.FirstDepartureTime} .... {FD1.FirstArrivalTime} <FaClock/>{FD1.SecondDepartureTime} .....  {FD1.FinalArrivalTime} </CardText>



                                    <CardText data-es-label="Price"> {FD1.firstLegDuration} <FaCloud/>{FD1.SecondLegDuration}</CardText>

                                    <CardText data-es-label="Date"> {FD1.Date}</CardText>

                                 <CardText data-es-label="Price" className="fa fa-euro" style={{fontSize:24}}> <b>{FD1.Price}</b></CardText>
                                    <CardText data-es-label="Price">Available seats at this price:  {FD1.routeAvailability}</CardText> {this.Select(FD1)}



                                </Card>
                            </div>
                        </div>
                    </div>
                </Card>
                </Row>
            )

        )
    }

    /**
     * Destiantion route for origin two
     * @param FS
     * @returns {*}
     */
    createCardsOriginTwo(FS){

        console.log(FS);
        var data4= FS[1];
        return data4.map( FD2 => (


                <Row style={{paddingTop: 10}}> <Card style={{backgroundColor:'#4db6ac'}} className="card" key={FD2.FlightID}>


                    <div className="text-center" style={{minWidth:350}}>
                        <h5><b> {FD2.FirstDepartCityName}</b></h5>
                        <div className="panel panel-default datasource_panel">
                            <div className="panel-heading">

                                <Card body className="text-center">

                                    <CardTitle data-es-label="location2" style={{paddingTop:5}}>
                                        <b> {FD2.Departure} </b>.....  <FaPlane/><b> {FD2.FirstArrival} </b><FaPlane/>..... <b>{FD2.FinalArrival} </b>   </CardTitle>

                                    <CardText data-es-label="Price">{FD2.FirstDepartureTime} .... {FD2.FirstArrivalTime} <FaClock/>{FD2.SecondDepartureTime} .... {FD2.FinalArrivalTime} </CardText>


                                    <CardText data-es-label="Price"> {FD2.firstLegDuration} <FaCloud/>{FD2.SecondLegDuration}</CardText>

                                    <CardText data-es-label="Date"> {FD2.Date}</CardText>
                                    <CardText data-es-label="Price" className="fa fa-euro" style={{fontSize:24}}>  <b>{FD2.Price}</b></CardText>



                                    <CardText data-es-label="Price">Available seats at this price:  {FD2.routeAvailability}</CardText>  {this.Select(FD2)}


                                </Card>
                            </div>
                        </div>
                    </div>
                </Card>
                </Row>
            )
        )
    }

    render() {

            return (
                <div >

<div>

                        <NavCart cart={this.state.cart}  handleCheckboxChange={this.handleCheckboxChange}/>
</div>

                    <div style={{paddingLeft: 550}}>
                        {this.loading()}
                    </div>
                    <div style={{ paddingLeft: 100, paddingRight: 100 , paddingTop:30, paddingBottom:10}}>

                            {this.createCards()}
                    </div>

                </div>

            );
        }


}
 export default DestinationPrices;

