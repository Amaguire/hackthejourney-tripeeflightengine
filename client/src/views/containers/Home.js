
/* eslint react/prop-types: 0 */


/**
 * This file renders the home page
 */
import React from 'react';
import { BrowserRouter, Route, Link ,Redirect} from "react-router-dom";
import PropTypes from 'prop-types';
import '../css/form.css';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import ClipLoader from 'react-spinners/ClipLoader';
import Loader from 'react-loader-spinner';
import {Button, Form, FormGroup, Label, Input, FormText, Container} from 'reactstrap';
import moment from 'moment';
import blue from "@material-ui/core/es/colors/blue";
import LoadingOverlay from 'react-loading-overlay';

import tripee2 from './tripee3.PNG';




 export class Home extends React.Component {

  constructor(props) {
    super(props);

    //initialises the variables
   this.state = { originOne: '', originTwo: '', redirectToResult: false, startDate: new Date(), route:'', loading:false, isActive:false};

      this.onSubmit= this.onSubmit.bind(this);
      this.handleChange= this.handleChange.bind(this);
      this.setMulti=this.setMulti.bind(this);
      this.loading=this.loading.bind(this);

    }

     /**
      * This returns a loader, the state is updated when the form is successfully submitted and set to true
      * @returns {*}
      */
     loading() {
         return (

             <div className='sweet-loading'>
                 <ClipLoader
                     sizeUnit={"px"}
                     size={300}
                     color={'#123abc'}
                     loading={this.state.loading}
                 />
             </div>
         )
     }


     /**
      * On change of input fields the state updates
      * @param e
      */
     onChange = (e) => {
    this.setState({
       originOne: e.target.value, originTwo: e.target.value, route:e.target.value});
    };



     setMulti  = event => {
         this.setState(
             {route: event.target.value});

     };

     /**
      * on submit of form the field values are set, and the loading spinner is set to true
      * @param e
      */
 onSubmit = (e) => {

     //this.loader();
     e.preventDefault();

     const { originOne, originTwo , route} = this.state;
    this.setState({ loading : true, isActive :true});

};

     /**
      * This function handles date value state
      * @param date
      */
 handleChange(date){
     this.setState({
         startDate:date
     });
 }


     /**
      * Renders the home search panel
      * @returns {*}
      */

  render() {



   return(


<div>

       <div className="w3-display-middle" style={{width: '65%'}}>
           <LoadingOverlay
               active={this.state.isActive}
               spinner={this.loading()}
               text='Finding your trip...'
           >

           <div className="w3-bar w3-white" style={{borderRadius: 15}}>
       <Container className=" w3-container" style={{borderRadius: 25}}>
           <div className="w3-row-padding">


<Form  style={{borderRadius: 25}} name="FlightForm" id="FlightForm"  method="POST" action="/search" onSubmit={this.onSubmit}  >


    <div  id="Inspo" className="text-center"> <h3><b>Organise your holiday with <img src={tripee2} alt="Logo" style={{maxHeight: 200, maxWidth:230}}/></b></h3></div>


      <div >
        <div className="w3-half" style={{paddingLeft: 5, paddingRight:5, paddingBottom:10}} >

          <label>FROM</label>
         <input  required  style={{paddingLeft: 5, paddingRight:5}}type='text' name='originOne' id="originOne"  onChange={this.setState.originOne}  className="form-control" placeholder="Departing from"/>
        </div>
        <div className="w3-half" style={{paddingLeft: 5, paddingRight:5, paddingBottom:10}}>
          <label>FROM</label>
          <input required style={{paddingLeft: 5, paddingRight:5}} name='originTwo' id="originTwo"  onChange={this.setState.originTwo}  className="form-control" type="text" placeholder="Departing from"/>
        </div>

<div style={{paddingTop:10, paddingLeft: 250}}>
  Departure Date  <DatePicker className="form-control" required minDate={new Date()} name='date' selected={this.state.startDate} onChange={this.handleChange}/>

</div>

<div>
    <div>
    <label>
 <input   defaultChecked className="w3-radio" name="direct" type="radio" value="true" selected={this.setState.route ===true} onChange={this.onChange}/>
           Direct  </label>
    </div>
    <div>
<label>
  <input className="w3-radio" name="direct" type="radio" value="false"  onChange={this.onChange} selected={this.setState.route===false}/>  Non Direct</label>
        <div className="text-right" style={{ paddingBottom:20, paddingRight:25 }}><button type="submit" style={{ borderRadius:5}} name="search" id="search" value="true"
                                                                                          onSubmit={this.loading()} className="w3-button w3-dark-grey ">Search for destinations</button></div>

    </div>

</div>

      </div>




</Form>
           </div>
       </Container>
           </div>
           </LoadingOverlay>
       </div>

</div>


        );
    }
   }

export default Home;
