import React from "react";
import Card from "./NavCart";
import {CardText, CardTitle} from "reactstrap";


export class NavBar extends React.Component {

    render() {
        return (

            <div className="w3-bar w3-white w3-border-bottom w3-xlarge">

                <a name="id" id='Home' className='w3-bar-item w3-button w3-text-blue w3-hover-blue'>

                    <div className='fa fa-map-marker w3-margin-right'> <b>TRIPEE</b></div></a>
                <a href='' className='w3-bar-item w3-button w3-right w3-hover-blue w3-text-grey'>
                </a> </div>

        );
    }
}
export default NavBar;

