import React from 'react';
import {FaGlobeAmericas} from 'react-icons/fa';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import Card from '@material-ui/core/Card';
//import CardActions from '@material-ui/core/CardActions';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

//import 'bootstrap/dist/css/bootstrap.css';
//import 'animate.css/animate.min.css';
import 'font-awesome/css/font-awesome.min.css';
import {CardText, CardTitle} from "reactstrap";
//import Button from "./DestinationPrices";
import {Router, Route, Link, RouteHandler} from 'react-router';
import {FaPlane} from 'react-icons/fa';

export class NavCart extends React.Component {


    render() {
        console.log('this is the cart');
        var data= this.props.cart;


        console.log("data" +JSON.stringify(data));
       // console.log(data.toString());


        return (

            <div className="Nav" style={{paddingLeft: 999, paddingTop:10}}>
            <Card body className="text-center" style={{maxWidth: 375, minWidth:375}}>

                <CardTitle ><h3><b>Flights Selected : </b></h3></CardTitle>
                <CardText>{this.getData()}</CardText>
             <div style={{paddingLeft: 90, paddingBottom:5}}>



                <a href="/booking">   <span style={{float:"left"}} className="fa fa-cart-plus fa-2x"> {data.length} </span>
                    <Button  className="Button" name="book" method="POST" onSubmit={this.onSubmit} id="book" style={{backgroundColor:'#29b6f6'}}>Proceed with booking</Button></a>
             </div>
            </Card>

            </div>
        );
    }


    getData(){

        var data= this.props.cart;
        console.log(this.props.cart[0]);

        return data.map(FS => (
            <div style={{ backgroundColor: '#ce93d8',}}>
                <b>{FS.Departure}</b>... <FaPlane/>... <b>{FS.FinalArrival} </b>: <h3 className="fa fa-euro"/><b>{FS.Price}</b>
            </div>
        )
        )


    }









}





export default {NavCart};