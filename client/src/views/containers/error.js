import React from 'react';
import { css } from '@emotion/core';
// First way to import

import Home from './Home';

// Another way to import
import ClipLoader from 'react-spinners/ClipLoader';
import {NavCart} from "./NavCart";
import {TouchRippleProps as center} from "@material-ui/core/ButtonBase/TouchRipple";

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

export class error extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (

            <div style={{paddingLeft:425, borderWidth:5, paddingTop:100,shadowColor: "#ff0000",borderColor:"#ff0000"}}>
            <div className= "text-center" style={{ maxWidth:600, borderWidth:5, borderRadius: 15, shadowColor: "#ff0000",  backgroundColor: "#ffad99", borderColor:"#ff0000"}}>

                <div style={{ shadowColor: "#ffad99",paddingTop: 5,borderColor:"#ff0000", paddingBottom:5}}><h5> Sorry no route suggestions for those dates, try another date! </h5></div>


            </div>
                <Home> </Home>
            </div>


        )
    }
}
export default {error};