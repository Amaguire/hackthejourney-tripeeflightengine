
import React from 'react';
import {Form, Card, Group, Label, Control, Row, Col, InputGroup, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { CardImg, CardText, CardBody,
    CardTitle, CardSubtitle } from 'reactstrap';
import DatePicker from 'react-datepicker';
import ClipLoader from "./Home";
import Autocomplete from 'react-google-autocomplete';

export class form extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = { originOne: '', originTwo: '', redirectToResult: false, startDate: new Date(), route:'', loader:false, validated:false};

        //this.onChange= this.onChange.bind(this);
        // this.onChange= this.onChange(this);
        this.onSubmit= this.onSubmit.bind(this);
        this.handleChange= this.handleChange.bind(this);
        this.setMulti=this.setMulti.bind(this);
        this.loading=this.loading.bind(this);

    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        this.setState({validated: true});
    }


    render(){

        return (
            <div style={{paddingTop:200, paddingLeft:400}}>
                <Row className="show-grid" float="center">
                <Card className="text-center">

                    <CardTitle style={{paddingTop:10}}>
                        <b> <h2>Find your destination with Tripee</h2></b>
                    </CardTitle>
                    <Card.Body>

                {this.Form()}
                    </Card.Body>
                </Card>
                </Row>
            </div>



        )
    }


    loading() {
        return (
            <div className='sweet-loading'>
                <ClipLoader
                    sizeUnit={"px"}
                    size={300}
                    color={'#15bcb5'}
                    loading={this.state.loading}
                />
            </div>
        )
    }



    onChange = (e) => {
        this.setState({
            originOne: e.target.value, originTwo: e.target.value});
    };


    setMulti  = event => {
        this.setState({route: event.target.value});

    };

    onSubmit = (e) => {

        //this.loader();
        e.preventDefault();

        const { originOne, originTwo ,redirectToResult} = this.state;
        this.setState({ loading : true});
        {this.loading()}
    };

    handleChange(date){
        this.setState({
            startDate:date
        });
    }







    Form() {

        const {validated} = this.state;
        return (


            <Form
                noValidate
                validated={validated}
                onSubmit={e => this.handleSubmit(e)}
            >
                <div >
                    <div className="w3-half" style={{paddingLeft: 5, paddingRight:5, paddingBottom:10}} >

                        <label>FROM</label>
                        <input required style={{paddingLeft: 5, paddingRight:5}}type='text' name='originOne' id="originOne"  onChange={this.setState.originOne}  className="w3-input w3-border" placeholder="Departing from"/>
                    </div>
                    <div className="w3-half" style={{paddingLeft: 5, paddingRight:5, paddingBottom:10}}>
                        <label>FROM</label>
                        <input style={{paddingLeft: 5, paddingRight:5}} name='originTwo' id="originTwo"  onChange={this.setState.originTwo}  className="w3-input w3-border" type="text" placeholder="Departing from"/>
                    </div>
                </div>
                <Form.Row>
                    <Form.Group as={Col} md="4" controlId="validationCustom01">
                        <Form.Label>From </Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Departing From"

                        />

                        <Autocomplete
                            style={{width: '90%'}}
                            onPlaceSelected={(place) => {
                                console.log(place);
                            }}
                            types={['(regions)']}
                            componentRestrictions={{country: "ru"}}
                        />
                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>From</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="Departing From"

                        />

                        <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>

                <Form.Row>
                    <Form.Group as={Col} md="6" controlId="validationCustom03">
                        <Form.Label>Departure Date</Form.Label>


                        <DatePicker name='date' selected={this.state.startDate} onChange={this.handleChange}/>

                        <Form.Control.Feedback type="invalid">
                            Please provide a valid Date
                        </Form.Control.Feedback>

                    </Form.Group>

                </Form.Row>
                <Form.Group>
                    <Form.Check
                        required
                        label="Agree to terms and conditions"
                        feedback="You must agree before submitting."
                    />
                </Form.Group>
                <Button type="submit">Submit form</Button>
            </Form>
        );
    }

    ;
}
export default {form};