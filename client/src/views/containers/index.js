/**
 * http://usejsdoc.org/
 */
import { default as Home } from './Home';
import { default as destinationPrices } from './DestinationPrices';
import { default as originCards } from './originCards';
import { default as NavCart} from  './NavCart';
import {default as NavBar} from './NavBar';

export { Home, destinationPrices, originCards, NavCart, NavBar };