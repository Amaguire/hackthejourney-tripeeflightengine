import React from 'react';
import { css } from '@emotion/core';
// First way to import

// Another way to import
import ClipLoader from 'react-spinners/ClipLoader';
import {NavCart} from "./NavCart";

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

export class loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }
    render() {
        return (
            <div>
            <div className='sweet-loading'>
                <ClipLoader
                    css={override}
                    sizeUnit={"px"}
                    size={150}
                    color={'#123abc'}
                    loading={this.state.loading}
                />
                <div id = "hidden" ></div></div>
            </div>


        )
    }
}
export default {loader};