/**
 * This file Generates  google maps
 */


import React  from 'react';
import { render } from 'react-dom';
import Card from "./DestinationPrices";

class MapContainer extends React.Component {
    constructor(props) {
        super(props);
        this.onScriptLoad = this.onScriptLoad.bind(this)
    }

    onScriptLoad() {

        const maps = new window.google.maps.Map(
            document.getElementById(this.props.id),
            this.props.options);
            this.props.onMapLoad(maps)
    }


    /**
     * THis method was used from the read me file in the module imported
     * https://www.npmjs.com/package/google-maps-react
     */
    componentDidMount() {

        console.log("THIS IS THE ID "+this.props.id);
        if (!window.google) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.src = `https://maps.google.com/maps/api/js?key=AIzaSyBDfSvsC56mB_ZQ976bwqRF3y_QQnKvn7s`;
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);

            s.addEventListener('load', e => {
                this.onScriptLoad()
            })
        } else {
            this.onScriptLoad()
        }
    }

    render() {
        return (
            <div style={{ width: 400, height: 400 }} id={this.props.id} />
        );
    }
}

export default MapContainer

