import React from 'react';
import Observable from 'redux-observable';
//import {Table} from 'react-bootstrap';
import { CardImg, CardText, CardBody,
    CardTitle, CardSubtitle } from 'reactstrap';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { Container, Row, Col } from 'reactstrap';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';



export class originCards extends React.Component {
    constructor() {
        super();
        this.state = {
            flightData: []
        };
    }

    componentDidMount() {

        fetch('/search')
            .then(res => {
                console.log('this is getting response on DesintationPrices page' + res);
                return res.json()

            })
            .then(res => {

                console.log(res.toString());


                this.setState({flightData: res});

            })
            .catch(error => console.log(error));
    }


    originTitleTwo() {
        var data = this.state.flightData;

        return data.map(FD => (
                <Card >
                    <Card.body>
                        <Card.Title>
                            {FD[0].FirstDepartCityName}
                        </Card.Title>
                    </Card.body>
                </Card>



            )
        )
    }

    originTitleOne() {
        var data = this.state.flightData;

        return data.map(FD => (
                <Card >
                    <Card.body>
                        <Card.Title>
                            {FD[0].FirstDepartCityName}
                        </Card.Title>
                    </Card.body>
                </Card>



            )
        )
    }
    render() {

        return (
            <div>

                <div>
                    {this.originTitleTwo() }
                    {this.originTitleOne() }
                </div>
            </div>

        );
    }


}
export default originCards;




