var fs = require('fs');

var DestinationData = require('../data/destinations3.json');
var DestinationData2 = require('../data/destinations4.json');
var DestinationList = require('../data/destinationList');
var express = require('express');
var bodyParser = require('body-parser');
var parseJson = require('parse-json');
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
var destList = require('dest-list-tripee');
var origin = require('tripee-origins');


var Amadeus = require('amadeus');
var amadeus = new Amadeus({
  hostname: 'production',
  clientId:    'WMA79ejcMbxGuWjhMgAVR6HqCG8oe0km',
  clientSecret: 'WLpXRkHZt9hzOksI'
});



var mongoose = require('mongoose');
mongoose.promise= global.Promise;
mongoose.connect('mongodb://localhost/Tripee',  { useNewUrlParser: true },  function (err, database) {
    if (err) throw err;
    else {
        db= database;
        console.log('Successfully connected to mongodb');
    }
});

var FinalArrival;
var FirstDeparture;
var Price;
var routeId;
var FirstDateTimeArrival;
var FirstDateTimeDepart;
var  SecondDateTimeArrival;
var  SecondDateTimeDepart;
var durationDirectOrLegOne;
var durationLeg2;
var firstLegavailabilty;
var secondLegavailabilty;
var firstArrival;
var FirstDepartCityName;
var secondDeparture;
var FinalArrivalCityName;
var  PriceArrayFinal = [];

var DestListArray=[];

module.exports.Destinations1GetAll = function(req, res) {

  console.log('GET the Destinations');
  console.log(req.query);

  var returnData;
  var offset = 0;
  var count = 5;

  if (req.query && req.query.offset) {
    offset = parseInt(req.query.offset, 10);
  }
  
  if (req.query && req.query.count) {
    count = parseInt(req.query.count, 10);
  }

  returnData = DestinationData.slice(offset,offset+count);

  res
    .status(200)
    .json(returnData);
};


module.exports.destination1GetPrice = function(req, res) {
    console.log('GET prices');

    var originOne = 'London';
    var originTwo = 'Paris';

    var IataCodeArrayOne = [];
    var IataCodeArrayTwo = [];


    var originCodeOne = db.collection("CityData").find({municipality: originOne}).toArray(function (err, result) {
        if (err) throw err;
     //   console.log(result);
        IataCodeArrayOne.push(result);
       // console.log('Iatacode array one: ' + IataCodeArrayOne);
    });

    var originCodeTwo = db.collection("CityData").find({municipality: originTwo}).toArray(function (err, result) {
        if (err) throw err;
      //  console.log(result);
        IataCodeArrayTwo.push(result);
        var stringIataCode1 = JSON.stringify(IataCodeArrayTwo);
      //  console.log('Iatacode array Two: ' + IataCodeArrayTwo);

        var arryIataOne = [];
        for (let i = 0, len = IataCodeArrayTwo.length; i < len; i++) {
            var iatArr = (IataCodeArrayTwo);
            arryIataOne.push(iatArr.iata_code);
        }
       // console.log(arryIataOne);
    });


    console.log('origins' + originOne + originTwo);


    var destdata = (DestinationData);
    var destdata2 = (DestinationData2);
    var listofDest = {DestinationList};

    var finalPriceArray = [];
    var priceArray = [];
    var priceArray2 = [];
    for (let i = 0, len = destdata.length; i < len; i++) {
        var priceElement = (destdata);
        priceArray.push(priceElement[i]);

    }

    for (let i = 0, len = destdata2.length; i < len; i++) {
        var priceElement2 = (destdata2);
        priceArray2.push(priceElement2[i]);

    }



        var mergedPrices = priceArray.concat(priceArray2);
        finalPriceArray.push(destdata, destdata2);


    for (let i = 0, len = mergedPrices.length; i < len; i++) {

        var DestElement = mergedPrices;

        var destObj = Object.keys(DestElement[i].offerItems[0].services[0].segments);
        var DestListArrayList={};

        if (destObj.hasOwnProperty('1')) {
            FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;
            DestListArrayList = (FinalArrival);
        }else{
            FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
            DestListArrayList = (FinalArrival);
        }

        DestListArray.push(DestListArrayList);

    }

    console.log(DestListArray);

    var s1= new Set(DestListArray);
    console.log(s1);


    for (let value of s1) {
        console.log(value);
    }





        for (let i = 0, len = mergedPrices.length; i < len; i++) {

            var DestElement = mergedPrices;

            var destObj = Object.keys(DestElement[i].offerItems[0].services[0].segments);




            var  newArray;

            if (destObj.hasOwnProperty('1')) {

                var CityPromise = new Promise(function (resolve, reject) {

                    firstArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;

                    db.collection("CityData").find({iata_code: FirstDeparture}).toArray(function (err, result) {
                        if (err) throw err;
                        //  var stringified=  JSON.stringify(result);

                        //   console.log('cityresult'+ result[0].municipality);
                        resolve(result);
                        // return FirstArrivalCityName = result[0].municipality;
                        db.close();
                    });
                });

                CityPromise.then(function (result) {
                    newArray={};

                    routeId = DestElement[i].id;
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;



                    //Date time for depart and arrival on first and second Leg
                    FirstDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at;
                    FirstDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at;
                    SecondDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.at;
                    SecondDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.at;


                    //duration of flights on both first and second leg
                    durationDirectOrLegOne = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                    durationLeg2 = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.duration;

                    secondDeparture = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.iataCode;

                    firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;
                    secondLegavailabilty = DestElement[i].offerItems[0].services[0].segments[1].pricingDetailPerAdult.availability;
                    Price = DestElement[i].offerItems[0].price.total;
                    // console.log('true');
                    FirstDepartCityName = result[0].municipality;


                    newArray["FlightID"] = (routeId);
                    newArray["Departure"] = (FirstDeparture);
                    newArray["FirstDepartureTime"] = (FirstDateTimeDepart);
                    newArray["FirstArrival"] = (firstArrival);
                    newArray["FirstArrivalTime"] = (FirstDateTimeArrival);
                    newArray["firstLegDuration"] = (durationDirectOrLegOne);
                    newArray["routeAvailability"] = (firstLegavailabilty);

                    newArray["SecondLegDeparture"] = (secondDeparture);
                    newArray["SecondDepartureTime"] = (SecondDateTimeDepart);
                    newArray["FinalArrival"] = (FinalArrival);
                    newArray["FinalArrivalTime"] = (SecondDateTimeArrival);
                    newArray["SecondLegDuration"] = (durationLeg2);
                    newArray["routeAvailability"] = (secondLegavailabilty);
                    newArray["Price"] = (Price);
                    newArray["FirstDepartCityName"] = (FirstDepartCityName);
                    console.log('this is arrivalCityName' + FirstDepartCityName);
                    PriceArrayFinal.push(newArray);


                })
            } else {

                var CityPromise2 = new Promise(function (resolve, reject) {

                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;

                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
                    var originCityOne = db.collection("CityData").find({iata_code:FirstDeparture}).toArray(function (err, result) {
                        if (err) throw err;
                        //  var stringified=  JSON.stringify(result);

                        // console.log('cityresult'+ result[0].municipality);
                        resolve(result);
                        //return FinalArrivalCityName = result[0].municipality;
                        db.close();

                    });

                });

                CityPromise2.then(function (result) {

                    newArray={};
                    routeId = DestElement[i].id;
                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;

                    Price = DestElement[i].offerItems[0].price.total;

                    FirstDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at;
                    FirstDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at;

                    durationDirectOrLegOne = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                    firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;
                    FirstDepartCityName = result[0].municipality;

                    newArray["FlightID"] = (routeId);
                    newArray["Departure"] = (FirstDeparture);
                    newArray["FirstDepartureTime"] = (FirstDateTimeDepart);

                    newArray["Arrival"] = (FinalArrival);
                    newArray["FirstArrivalTime"] = (FirstDateTimeArrival);
                    newArray["Price"] = (Price);
                    newArray["firstLegDuration"] = (durationDirectOrLegOne);
                    newArray["routeAvailability"] = (firstLegavailabilty);
                    newArray["FirstDepartCityName"] = (FirstDepartCityName);

                    console.log('this is arrivalCityName' + FirstDepartCityName);
                    PriceArrayFinal.push(newArray);


                });

            }

            setTimeout(function (){
                var FlightDataStringify = JSON.stringify(PriceArrayFinal);
                console.log(PriceArrayFinal);

                console.log(FlightDataStringify);
                fs.writeFileSync('api/data/destinations5.json', FlightDataStringify);
                res.send(FlightDataStringify);

            }, 10000);


        }




};


