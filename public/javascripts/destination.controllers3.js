var fs = require('fs');

var DestinationData = require('../data/destinations3.json');
var DestinationData2 = require('../data/destinations4.json');
var DestinationList = require('../data/destinationList');
var express = require('express');
var bodyParser = require('body-parser');
var parseJson = require('parse-json');
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false }));
var destList = require('dest-list-tripee');
var origin = require('tripee-origins');



var mongoose = require('mongoose');
mongoose.promise= global.Promise;
mongoose.connect('mongodb://localhost/Tripee',  { useNewUrlParser: true },  function (err, database) {
    if (err) throw err;
    else {
        db= database;
        console.log('Successfully connected to mongodb');
    }
});


module.exports.destination1GetPrice = function(req, res) {
    console.log('GET prices');

    var FinalArrival;
    var FirstDeparture;
    var Price;
    var routeId;
    var FirstDateTimeArrival;
    var FirstDateTimeDepart;
    var SecondDateTimeArrival;
    var SecondDateTimeDepart;
    var durationDirectOrLegOne;
    var durationLeg2;
    var firstLegavailabilty;
    var secondLegavailabilty;
    var firstArrival;
    var FirstDepartCityName;
    var secondDeparture;
    var FinalArrivalCityName;
    var PriceArrayFinal = [];
    var cityNameMapArray = [];
    var cityOriginNameMapArray = [];
    var CityNameOrigin = [];
    var DestListArray = [];
    var OriginArray = [];
    var FlightDataStringify;
    var myMap;
    var CityName;
    var mapArray = [];
    var FinalOriginArray = [];


    var destdata = (DestinationData);
    var destdata2 = (DestinationData2);
    var listofDest = {DestinationList};

    var finalPriceArray = [];
    var priceArray = [];
    var priceArray2 = [];

    for (let i = 0, len = destdata.length; i < len; i++) {
        var priceElement = (destdata);
        priceArray.push(priceElement[i]);

    }

    for (let i = 0, len = destdata2.length; i < len; i++) {
        var priceElement2 = (destdata2);
        priceArray2.push(priceElement2[i]);

    }


    var mergedPrices = priceArray.concat(priceArray2);
    finalPriceArray.push(destdata, destdata2);


    for (let i = 0, len = mergedPrices.length; i < len; i++) {

        var DestElement = mergedPrices;

        var destObj = Object.keys(DestElement[i].offerItems[0].services[0].segments);
        var DestListArrayList = {};

        if (destObj.hasOwnProperty('1')) {
            FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;
            DestListArrayList = (FinalArrival);
        } else {
            FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
            DestListArrayList = (FinalArrival);
        }

        DestListArray.push(DestListArrayList);

    }

    for (let i = 0, len = mergedPrices.length; i < len; i++) {

        var DestElement2 = mergedPrices;

        var destObj2 = Object.keys(DestElement2[i].offerItems[0].services[0].segments);
        var OriginArrayList = {};

        FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
        OriginArrayList = (FirstDeparture);


        OriginArray.push(OriginArrayList);

    }


    console.log(DestListArray);

    var s1 = new Set(DestListArray);
    console.log(s1);

    var s2 = new Set(OriginArray);
    console.log(s2);

    myMap = new Map();
    var CityPromise3 = new Promise(function (resolve, reject) {

        for (let value of s1) {
            db.collection("CityData").find({iata_code: value}).toArray(function (err, result) {
                if (err) throw err;

                //console.log(result[0].municipality);
                CityName = result[0].municipality;
                console.log(CityName);
                console.log('name');
                cityNameMapArray.push(CityName);
                resolve(cityNameMapArray);

            });

        }

    });

    var CityOriginPromise = new Promise(function (resolve, reject) {

        for (let value of s2) {
            db.collection("CityData").find({iata_code: value}).toArray(function (err, result) {
                if (err) throw err;

                //console.log(result[0].municipality);
                CityNameOrigin = result[0].municipality;

                //  console.log(CityNameOrigin);
                cityOriginNameMapArray.push(CityNameOrigin);

                // console.log(CityNameOrigin);
                resolve(cityOriginNameMapArray);

            });

        }

    });

    CityOriginPromise.then(function () {
        console.log('CityNme' + cityOriginNameMapArray);
    });

    CityPromise3.then(function () {
        console.log(cityNameMapArray);
    });

    for (let i = 0, len = mergedPrices.length; i < len; i++) {

        var DestElement = mergedPrices;

        var destObj = Object.keys(DestElement[i].offerItems[0].services[0].segments);


        var FlightData;


        if (destObj.hasOwnProperty('1')) {
            var CityPromise = new Promise(function (resolve, reject) {

                firstArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;

                db.collection("CityData").find({iata_code: FirstDeparture}).toArray(function (err, result) {
                    if (err) throw err;


                    FinalArrivalCityName = result[0].municipality;

                    db.close();
                });
                    db.collection("CityData").find({iata_code: FinalArrival}).toArray(function (err, res2) {
                        if (err) throw err;

                        FirstDepartCityName = res2[0].municipality;

                        resolve();


                    });

                });

                CityPromise.then(function (result) {

                    FlightData = {};
                    firstArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;

                    routeId = DestElement[i].id;
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.iataCode;


                    //Date time for depart and arrival on first and second Leg
                    FirstDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at;
                    FirstDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at;
                    SecondDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.arrival.at;
                    SecondDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.at;


                    //duration of flights on both first and second leg
                    durationDirectOrLegOne = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                    durationLeg2 = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.duration;

                    secondDeparture = DestElement[i].offerItems[0].services[0].segments[1].flightSegment.departure.iataCode;

                    firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;
                    secondLegavailabilty = DestElement[i].offerItems[0].services[0].segments[1].pricingDetailPerAdult.availability;
                    Price = DestElement[i].offerItems[0].price.total;
                    // console.log('true');
                    //  FirstDepartCityName = result[0].municipality;


                    FlightData["FlightID"] = (routeId);
                    FlightData["Departure"] = (FirstDeparture);
                    FlightData["FirstDepartureTime"] = (FirstDateTimeDepart);
                    FlightData["FirstArrival"] = (firstArrival);
                    FlightData["FirstArrivalTime"] = (FirstDateTimeArrival);
                    FlightData["firstLegDuration"] = (durationDirectOrLegOne);
                    FlightData["routeAvailability"] = (firstLegavailabilty);

                    FlightData["SecondLegDeparture"] = (secondDeparture);
                    FlightData["SecondDepartureTime"] = (SecondDateTimeDepart);
                    FlightData["FinalArrival"] = (FinalArrival);
                    FlightData["FinalArrivalTime"] = (SecondDateTimeArrival);
                    FlightData["SecondLegDuration"] = (durationLeg2);
                    FlightData["routeAvailability"] = (secondLegavailabilty);
                    FlightData["Price"] = (Price);
                    FlightData["FirstDepartCityName"] = (FirstDepartCityName);
                    FlightData["FinalArrivalCity"] = (FinalArrivalCityName);

                    // console.log('this is arrivalCityName' + FirstDepartCityName);


                    PriceArrayFinal.push(FlightData);

                })

        } else {
            var CityPromise2 = new Promise(function (resolve, reject) {

                FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;

                FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;


                var originCityOne = db.collection("CityData").find({iata_code: FirstDeparture}).toArray(function (err, result) {
                    if (err) throw err;

                    FirstDepartCityName = result[0].municipality;


                });
                    db.collection("CityData").find({iata_code: FinalArrival}).toArray(function (err, res2) {
                        if (err) throw err;

                        FinalArrivalCityName = res2[0].municipality;

                        resolve();


                    });

                });
                CityPromise2.then(function (result) {

                    FlightData = {};
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;
                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;

                    routeId = DestElement[i].id;
                    FirstDeparture = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.iataCode;
                    FinalArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.iataCode;

                    Price = DestElement[i].offerItems[0].price.total;

                    FirstDateTimeArrival = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.arrival.at;
                    FirstDateTimeDepart = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.departure.at;

                    durationDirectOrLegOne = DestElement[i].offerItems[0].services[0].segments[0].flightSegment.duration;
                    firstLegavailabilty = DestElement[i].offerItems[0].services[0].segments[0].pricingDetailPerAdult.availability;
                    //FirstDepartCityName = result[0].municipality;

                    //   FinalArrivalCityName= CityName;

                    FlightData["FlightID"] = (routeId);
                    FlightData["Departure"] = (FirstDeparture);
                    FlightData["FirstDepartureTime"] = (FirstDateTimeDepart);

                    FlightData["FinalArrival"] = (FinalArrival);
                    FlightData["FirstArrivalTime"] = (FirstDateTimeArrival);
                    FlightData["Price"] = (Price);
                    FlightData["firstLegDuration"] = (durationDirectOrLegOne);
                    FlightData["routeAvailability"] = (firstLegavailabilty);
                    FlightData["FirstDepartCityName"] = (FirstDepartCityName);
                    FlightData["FinalArrivalCity"] = (FinalArrivalCityName);
                    //  console.log('this is arrivalCityName' + FirstDepartCityName);
                    PriceArrayFinal.push(FlightData);

                    FlightDataStringify = JSON.stringify(PriceArrayFinal);
                });

        }

    }


    function groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
            const key = keyGetter(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
        return map;
    }


    var FA = [];
    var FA2 = [];
    var Total = [];
    var T = [];
    setTimeout(function () {
        //console.log(cityNameMapArray);
        var s3 = new Set(cityOriginNameMapArray);
        console.log(s3);


        for (let value of s1) {
            const routeGrouped = groupBy(PriceArrayFinal, route => route.FinalArrival);
            //  console.log(value + ': ' + routeGrouped.get(value));

            var originArray = [];
            for (let origin of s3) {
                const originGrouped = groupBy(routeGrouped.get(value), route => route.FirstDepartCityName);
                console.log(origin + ': ' + originGrouped.get(origin));

                var originT = originGrouped.get(origin);
                originArray.push(originT);


            }
            console.log(originArray);
            FA2.push(originArray);
            FA.push(FA2);

        }


        Total.push(FA);
        console.log(Total);
        fs.writeFileSync('api/data/destinations5.json', FlightDataStringify);
        res.send(Total);
    }, 7000);




};





