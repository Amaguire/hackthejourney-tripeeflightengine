import {describe} from 'mocha';


var assert = require('assert');
var SearchController =  require('./search.controllers.js');

var expect = require('chai').expect;
var should = require('chai').should();

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised).should();


var controller = require('../../controllers/welcome');
    var http_mocks = require('node-mocks-http');
    var should = require('should');

function buildResponse() {
    return http_mocks.createResponse({eventEmitter: require('events').EventEmitter})
}

describe('Search Controller Tests', function() {

    it('Search', function (done) {
        var response = buildResponse();
        var request = http_mocks.createRequest({
            method: 'GET',
            url: '/search',
        });

        response.on('end', function () {
            response._getData().should.be.an('array');
            done()
        });

        controller.handle(request, response)
    });


});